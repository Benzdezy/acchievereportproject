(self.webpackChunk_N_E=self.webpackChunk_N_E||[]).push([[405],{8456:function(e,r,t){"use strict";t.d(r,{Z:function(){return j}});var o=t(3366),i=t(7462),n=t(7294),a=t(6010),s=t(4780),c=t(917),l=t(8216),u=t(1657),d=t(948),f=t(1588),m=t(4867);function h(e){return(0,m.Z)("MuiCircularProgress",e)}(0,f.Z)("MuiCircularProgress",["root","determinate","indeterminate","colorPrimary","colorSecondary","svg","circle","circleDeterminate","circleIndeterminate","circleDisableShrink"]);var _=t(5893);let p=["className","color","disableShrink","size","style","thickness","value","variant"],v=e=>e,g,x,k,Z,y=(0,c.F4)(g||(g=v`
  0% {
    transform: rotate(0deg);
  }

  100% {
    transform: rotate(360deg);
  }
`)),w=(0,c.F4)(x||(x=v`
  0% {
    stroke-dasharray: 1px, 200px;
    stroke-dashoffset: 0;
  }

  50% {
    stroke-dasharray: 100px, 200px;
    stroke-dashoffset: -15px;
  }

  100% {
    stroke-dasharray: 100px, 200px;
    stroke-dashoffset: -125px;
  }
`)),b=e=>{let{classes:r,variant:t,color:o,disableShrink:i}=e,n={root:["root",t,`color${(0,l.Z)(o)}`],svg:["svg"],circle:["circle",`circle${(0,l.Z)(t)}`,i&&"circleDisableShrink"]};return(0,s.Z)(n,h,r)},C=(0,d.ZP)("span",{name:"MuiCircularProgress",slot:"Root",overridesResolver:(e,r)=>{let{ownerState:t}=e;return[r.root,r[t.variant],r[`color${(0,l.Z)(t.color)}`]]}})(({ownerState:e,theme:r})=>(0,i.Z)({display:"inline-block"},"determinate"===e.variant&&{transition:r.transitions.create("transform")},"inherit"!==e.color&&{color:(r.vars||r).palette[e.color].main}),({ownerState:e})=>"indeterminate"===e.variant&&(0,c.iv)(k||(k=v`
      animation: ${0} 1.4s linear infinite;
    `),y)),N=(0,d.ZP)("svg",{name:"MuiCircularProgress",slot:"Svg",overridesResolver:(e,r)=>r.svg})({display:"block"}),P=(0,d.ZP)("circle",{name:"MuiCircularProgress",slot:"Circle",overridesResolver:(e,r)=>{let{ownerState:t}=e;return[r.circle,r[`circle${(0,l.Z)(t.variant)}`],t.disableShrink&&r.circleDisableShrink]}})(({ownerState:e,theme:r})=>(0,i.Z)({stroke:"currentColor"},"determinate"===e.variant&&{transition:r.transitions.create("stroke-dashoffset")},"indeterminate"===e.variant&&{strokeDasharray:"80px, 200px",strokeDashoffset:0}),({ownerState:e})=>"indeterminate"===e.variant&&!e.disableShrink&&(0,c.iv)(Z||(Z=v`
      animation: ${0} 1.4s ease-in-out infinite;
    `),w)),S=n.forwardRef(function(e,r){let t=(0,u.Z)({props:e,name:"MuiCircularProgress"}),{className:n,color:s="primary",disableShrink:c=!1,size:l=40,style:d,thickness:f=3.6,value:m=0,variant:h="indeterminate"}=t,v=(0,o.Z)(t,p),g=(0,i.Z)({},t,{color:s,disableShrink:c,size:l,thickness:f,value:m,variant:h}),x=b(g),k={},Z={},y={};if("determinate"===h){let w=2*Math.PI*((44-f)/2);k.strokeDasharray=w.toFixed(3),y["aria-valuenow"]=Math.round(m),k.strokeDashoffset=`${((100-m)/100*w).toFixed(3)}px`,Z.transform="rotate(-90deg)"}return(0,_.jsx)(C,(0,i.Z)({className:(0,a.Z)(x.root,n),style:(0,i.Z)({width:l,height:l},Z,d),ownerState:g,ref:r,role:"progressbar"},y,v,{children:(0,_.jsx)(N,{className:x.svg,ownerState:g,viewBox:"22 22 44 44",children:(0,_.jsx)(P,{className:x.circle,style:k,ownerState:g,cx:44,cy:44,r:(44-f)/2,fill:"none",strokeWidth:f})})}))});var j=S},5557:function(e,r,t){(window.__NEXT_P=window.__NEXT_P||[]).push(["/",function(){return t(8287)}])},8287:function(e,r,t){"use strict";t.r(r),t.d(r,{default:function(){return f}});var o=t(5893),i=t(9008),n=t.n(i),a=t(7294),s=t(1163),c=t(8456),l=t(3915),u=t.n(l),d=t(7041);function f(){let e=(0,s.useRouter)();(0,a.useEffect)(()=>{let e=(0,d.getCookie)("UserID");r("".concat(e))},[]);let r=async r=>{let t=await fetch("https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile",{method:"GET",headers:{Authorization:"".concat(r)}}),o=await t.json();if(!1==o.status)e.push("/login");else if(!0==o.status){let i=o.data.UserType;console.log(i),"Customer"==i?(console.log("Verify successfuly!! Customer"),e.push("/viewreport")):"Admin"==i&&(console.log("Verify successfuly!! Admin"),e.push("/managecomp"))}};return(0,o.jsxs)("div",{children:[(0,o.jsxs)(n(),{children:[(0,o.jsx)("title",{children:"LIFF Next App"}),(0,o.jsx)("link",{rel:"icon",href:"/favicon.ico"})]}),(0,o.jsx)("div",{children:(0,o.jsx)("div",{className:u().container,children:(0,o.jsx)(c.Z,{})})})]})}},3915:function(e){e.exports={container:"Home_container__Ennsq",main:"Home_main__EtNt2",footer:"Home_footer__7dKhS",title:"Home_title__FX7xZ",description:"Home_description__Qwq1f",code:"Home_code__aGV0U",grid:"Home_grid__c_g6N",card:"Home_card__7oz7W",logo:"Home_logo__80mSr",bar:"Home_bar__FKJEe",btn_logout:"Home_btn_logout__WheFn"}},9008:function(e,r,t){e.exports=t(3121)}},function(e){e.O(0,[928,774,888,179],function(){return e(e.s=5557)}),_N_E=e.O()}]);