import { Container } from "@mui/system";
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, FormGroup, FormControlLabel, FormControl, FormLabel, Stack, Autocomplete, styled, DialogTitle, Dialog, DialogActions } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import Checkbox from '@mui/material/Checkbox';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useEffect, useState } from "react";
import { getCookie } from "cookies-next";


const EditCompany = (props) => {
    const { onClickBack, data, getCompany, listDatabase, listDatabasePermission, getDatabasePermission, handleEditCompDeletePermission } = props;

    let arrayIndexData = [];
    const filterUidCompany = listDatabasePermission.filter(permission => {
        return permission.CompanyUID === data.CompanyUID
    })
    if (filterUidCompany.length !== 0) {
        for (let i = 0; i < listDatabase.length; i++) {
            for (let j = 0; j < filterUidCompany.length; j++) {
                if (listDatabase[i].DatabaseID == filterUidCompany[j].DatabaseID) {
                    arrayIndexData.push(listDatabase[i])
                }
            }
        }
    }

    const [value, setValue] = useState(arrayIndexData.length != 0 ? arrayIndexData : undefined);
    const [nameCompany, setNameCompany] = useState(data.CompanyName)
    const [remarkCompany, setRemarkCompany] = useState(data.Remark)
    const [uidCompany, setUidCompany] = useState(data.CompanyUID)
    const [openDeleteAlert, setOpenDeleteAlert] = useState(false);
    const [checkedSoftware, setCheckedSoftware] = useState(data.CompanyType.includes("Software") ? true : false);
    const [checkedAccount, setCheckedAccount] = useState(data.CompanyType.includes("Account") ? true : false)

    console.log("value", value)



    const handleChangeSoftware = (event) => {
        setCheckedSoftware(event.target.checked);
    };


    const handleChangeAccount = (event) => {
        setCheckedAccount(event.target.checked);
    };

    const handleClickSubmit = () => {
        getCompany()
        getDatabasePermission()
        onClickBack("MainPageManageComp")
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const AccessToken = getCookie('AccessToken');
        let checkboxValue = ''
        if (checkedAccount == true && checkedSoftware == false) {
            checkboxValue = 'Account'
        } else if (checkedAccount == false && checkedSoftware == true) {
            checkboxValue = 'Software'
        } else if (checkedAccount == true && checkedSoftware == true) {
            checkboxValue = 'Account,Software'
        }
        const data = {
            CompanyName: nameCompany,
            CompanyType: checkboxValue,
            Remark: remarkCompany
        }
        const JSONdata = JSON.stringify(data)
        const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company?CompanyUID=${uidCompany}`

        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }

        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == true) {
            console.log("Edit company success!!")
            getDataCompany()
        }
    }

    const getDataCompany = async () => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company?DocStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            if (value.length !== 0) {
                managelights(result.data)
            } else {
                handleClickSubmit()
            }
        }
    }

    const managelights = (listcompany) => {
        const itemlights = listcompany.filter(company => {
            return company.CompanyName == nameCompany
        })
        const DatabaseID = value.map((item) => {
            return item.DatabaseID
        })
        addDatabasePermission(itemlights[0].CompanyUID, DatabaseID)
    }

    const addDatabasePermission = async (CompanyUID, DatabaseID) => {
        const AccessToken = getCookie('AccessToken');
        const data = {
            "CompanyUID": CompanyUID,
            "DatabaseID": DatabaseID
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/addDatabasePermission'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log("Add Permission result ==> ", result)
        if (result.status == true) {
            console.log('Add Permission Pass', result)
            handleClickSubmit()

        } else {
            console.log('Add Permission Error', result)
        }
    }

    const handleDeleteDatabasePermission = async () => {
        const AccessToken = getCookie('AccessToken');
        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
        }
        const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/deleteDatabasePermission?CompanyUID=${uidCompany}`
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == true) {
            console.log('Delete Permission Pass', result)
            setOpenDeleteAlert(false);
            handleEditCompDeletePermission()

        } else {
            console.log('Delete Permission Error', result)
        }
    }

    const StyledDialog = styled(Dialog)`
        .MuiBackdrop-root {
            background-color: #6a6a6a66;
        },
        .MuiPaper-root {
            box-shadow: none
        }
    `;

    const handleOpenDeleteAlert = (list) => {
        setOpenDeleteAlert(true);
    };

    const handleCloseDeleteAlert = () => {
        setOpenDeleteAlert(false);
    };

    const handleClickDelete = async () => {
        handleDeleteDatabasePermission()
    }

    return (
        <Box>
            <AppBar
                position="static"
                sx={{ bgcolor: '#FFFFFF' }}
            >

                <Toolbar sx={{ alignContent: 'center', justifyContent: 'space-between' }}>

                    <IconButton
                        sx={{ border: 1, borderRadius: 1, borderColor: '#097FF5', color: '#097FF5' }}
                        onClick={() => handleClickSubmit()}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>แก้ไขบริษัท</Typography>
                    <IconButton sx={{ border: 1, borderRadius: 0, borderColor: '#fff', color: '#fff' }} >
                        <ArrowBackIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh', p: 2 }}>
                    <form onSubmit={handleSubmit}>
                        <Box>
                            <TextField
                                fullWidth
                                label="ชื่อบริษัท"
                                variant="outlined"
                                value={nameCompany}
                                onChange={(e) => setNameCompany(e.target.value)}
                                required
                            />
                        </Box>
                        <Box >
                            <FormControl component="fieldset" sx={{ pt: 2 }} >
                                <FormGroup row sx={{ color: 'gray' }} >
                                    <FormControlLabel
                                        control={<Checkbox
                                            checked={checkedAccount}
                                            onChange={handleChangeAccount}
                                        />}
                                        label="บัญชี"
                                        labelPlacement="end"

                                    />
                                    <FormControlLabel
                                        control={<Checkbox
                                            checked={checkedSoftware}
                                            onChange={handleChangeSoftware}
                                        />}
                                        label="คอมพิวเตอร์"
                                        labelPlacement="end"
                                    />
                                </FormGroup>
                            </FormControl>
                        </Box>
                        <Box sx={{ py: 2 }}>
                            <Autocomplete
                                multiple
                                value={value || []}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                                options={listDatabase}
                                getOptionLabel={(option) => option.DatabaseName}
                                // filterSelectedOptions
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="เลือกฐานข้อมูล"
                                    />
                                )}
                            />
                            <Button
                                variant="contained"
                                sx={{ display: 'flex', mt: '10px' }}
                                onClick={() => handleOpenDeleteAlert()}
                            >
                                ลบการเชื่อมต่อฐานข้อมูลทั้งหมด
                            </Button>
                            <StyledDialog
                                open={openDeleteAlert}
                                onClose={handleCloseDeleteAlert}
                            >
                                <DialogTitle>
                                    {`ผู้ใช้ต้องการลบการเชื่อมต่อฐานข้อมูลทั้งหมดหรือไม่`}
                                </DialogTitle>
                                <DialogActions>
                                    <Button onClick={handleCloseDeleteAlert}>ยกเลิก</Button>
                                    <Button onClick={handleClickDelete} autoFocus>
                                        ยืนยัน
                                    </Button>
                                </DialogActions>
                            </StyledDialog>
                        </Box>
                        <Box>
                            <TextField
                                fullWidth
                                label="หมายเหตุ"
                                multiline
                                rows={4}
                                value={remarkCompany}
                                onChange={(e) => setRemarkCompany(e.target.value)}
                            />
                        </Box>
                        <Box sx={{ pt: 5 }}>
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                type="submit"
                            >บันทึก</Button>
                        </Box>
                    </form>
                </Box>
            </Container>
        </Box>
    )
}
export default EditCompany;