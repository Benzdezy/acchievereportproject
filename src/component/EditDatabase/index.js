import { Container } from "@mui/system";
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, FormGroup, FormControlLabel, FormControl, FormLabel, Stack, Autocomplete, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import Checkbox from '@mui/material/Checkbox';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState } from "react";
import Router from "next/router";
import { getCookie } from "cookies-next";
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import VisibilityIcon from '@mui/icons-material/Visibility';


const EditDatabase = (props) => {
    const { onClickBack, data, getListDatabase } = props;
    const router = Router;
    const [value, setValue] = useState([]);
    const [idDatabase, setIdDatabase] = useState(data.DatabaseID)
    const [nameDatabase, setNameDatabase] = useState(data.DatabaseName ? data.DatabaseName : '');
    const [noteDatabase, setNoteDatabase] = useState(data.DatabaseNote ? data.DatabaseNote : '')
    const [hostDatabase, setHostDatabase] = useState(data.DatabaseHost ? data.DatabaseHost : '');
    const [portDatabase, setPortDatabase] = useState(data.DatabasePort ? data.DatabasePort : '');
    const [userDatabase, setUserDatabase] = useState(data.DatabaseUser ? data.DatabaseUser : '');
    const [passwordDatabase, setPasswordDatabase] = useState(data.DatabasePassword ? data.DatabasePassword : '');
    const [showPassword, setShowPassword] = useState(false);
    const [open, setOpen] = useState(false);
    const [passwordUnlockDialog, setPasswordUnlockDialog] = useState('')
    const [statusReadOnly, setStatusReadOnly] = useState(true)

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    // const handleClickShowPassword = () => setShowPassword((show) => !show);
    const handleMouseDownPassword = (event) => {
        event.preventDefault();
        console.log('onMouseDown')
        if (showPassword == true) {
            setShowPassword(false)
            setStatusReadOnly(true)
        } else {
            handleClickOpen()
        }

    };

    const handleClickSubmit = () => {
        getListDatabase()
        onClickBack("MainPageManageDatabase")
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const AccessToken = getCookie('AccessToken');
        const data = {
            DatabaseName: nameDatabase,
            DatabaseNote: noteDatabase,
            DatabasePort: portDatabase,
            DatabaseHost: hostDatabase,
            DatabaseUser: userDatabase,
            DatabasePassword: passwordDatabase,
        }
        const JSONdata = JSON.stringify(data)

        const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DatabaseID=${idDatabase}`

        const options = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }

        const response = await fetch(endpoint, options)

        const result = await response.json()
        console.log("result EditDatabase ==> ", result)
        if (result.status == true) {
            handleClickSubmit()
        }
    }

    const handleSubmitPassword = async (event) => {
        event.preventDefault()
        if (passwordUnlockDialog == 'aswdev@2023') {
            setStatusReadOnly(false)
            setShowPassword(true)
            setPasswordUnlockDialog('')
            handleClose()
        } else {
            handleClose()
        }
        // const AccessToken = getCookie('AccessToken');
        // const data = {
        //     DatabaseName: nameDatabase,
        //     DatabaseNote: noteDatabase,
        //     DatabasePort: portDatabase,
        //     DatabaseHost: hostDatabase,
        //     DatabaseUser: userDatabase,
        //     DatabasePassword: passwordDatabase,
        // }
        // const JSONdata = JSON.stringify(data)

        // const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DatabaseID=${idDatabase}`

        // const options = {
        //     method: 'PUT',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Bearer ${AccessToken}`
        //     },
        //     body: JSONdata,
        // }

        // const response = await fetch(endpoint, options)

        // const result = await response.json()
        // console.log("result EditDatabase ==> ", result)
        // if (result.status == true) {
        //     handleClickSubmit()
        // }
    }

    return (
        <Box>
            <AppBar
                position="static"
                sx={{ bgcolor: '#FFFFFF' }}
            >

                <Toolbar sx={{ alignContent: 'center', justifyContent: 'space-between' }}>

                    <IconButton
                        sx={{ border: 1, borderRadius: 1, borderColor: '#097FF5', color: '#097FF5' }}
                        onClick={() => onClickBack("MainPageManageDatabase")}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>แก้ไขฐานข้อมูล</Typography>
                    <IconButton sx={{ border: 1, borderRadius: 0, borderColor: '#fff', color: '#fff' }} >
                        <ArrowBackIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh', p: 2 }}>
                    <form onSubmit={handleSubmit}>
                        <Box>
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Database name"
                                variant="outlined"
                                type="text"
                                id="databaseName"
                                required
                                value={nameDatabase}
                                onChange={(e) => setNameDatabase(e.target.value)}
                            />
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Database Note"
                                variant="outlined"
                                type="text"
                                id="databaseNote"
                                required
                                value={noteDatabase}
                                onChange={(e) => setNoteDatabase(e.target.value)}
                            />
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Host"
                                variant="outlined"
                                type="text"
                                id="databaseHost"
                                required
                                value={hostDatabase}
                                onChange={(e) => setHostDatabase(e.target.value)}
                            />
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Port"
                                variant="outlined"
                                type="text"
                                id="databasePort"
                                required
                                value={portDatabase}
                                onChange={(e) => setPortDatabase(e.target.value)}
                            />
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="User name"
                                variant="outlined"
                                type={showPassword ? 'text' : 'password'}
                                id="databaseUserName"
                                required
                                value={userDatabase}
                                onChange={(e) => setUserDatabase(e.target.value)}
                                InputProps={{
                                    endAdornment: (
                                        <>
                                            <IconButton
                                                position="end"
                                                // onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}>
                                                {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                                            </IconButton>
                                        </>
                                    ),
                                    readOnly: statusReadOnly,
                                }}
                            />
                            <TextField
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Password"
                                variant="outlined"
                                type={showPassword ? 'text' : 'password'}
                                id="databasePassword"
                                required
                                value={passwordDatabase}
                                onChange={(e) => setPasswordDatabase(e.target.value)}
                                InputProps={{
                                    endAdornment: (
                                        <>
                                            <IconButton
                                                position="end"
                                                // onClick={handleClickShowPassword}
                                                onMouseDown={handleMouseDownPassword}>
                                                {showPassword ? <VisibilityOffIcon /> : <VisibilityIcon />}
                                            </IconButton>
                                        </>
                                    ),
                                    readOnly: statusReadOnly,
                                }}
                            />
                            <Dialog open={open} onClose={handleClose}>
                                <DialogTitle display={'flex'} sx={{ mx: 'auto' }}>Verify</DialogTitle>
                                <DialogContent>
                                    <TextField
                                        autoFocus
                                        margin="dense"
                                        id="password"
                                        label="Password"
                                        type="text"
                                        fullWidth
                                        variant="outlined"
                                        value={passwordUnlockDialog}
                                        onChange={(e) => setPasswordUnlockDialog(e.target.value)}
                                    />
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={handleClose}>Cancel</Button>
                                    <Button onClick={handleSubmitPassword}>Submit</Button>
                                </DialogActions>
                            </Dialog>
                        </Box>
                        <Box sx={{ pt: 5 }}>
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                type="submit"

                            >บันทึก</Button>
                        </Box>
                    </form>
                </Box>
            </Container>
        </Box>
    )
}
export default EditDatabase;