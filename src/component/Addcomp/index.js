import { Container } from "@mui/system";
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, FormGroup, FormControlLabel, FormControl, FormLabel, Stack, Autocomplete } from "@mui/material";
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Checkbox from '@mui/material/Checkbox';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState } from "react";
import { getCookie } from "cookies-next";


const AddCompany = (props) => {
    const { onClickBack, getCompany, listDatabase, getDatabasePermission } = props;
    const [value, setValue] = useState([]);
    const [checkedSoftware, setCheckedSoftware] = useState(false);
    const [nameCompany, setNameCompany] = useState()
    const [remarkCompany, setRemarkCompany] = useState()
    const handleChangeSoftware = (event) => {
        setCheckedSoftware(event.target.checked);
    };

    const [checkedAccount, setCheckedAccount] = useState(false);
    const handleChangeAccount = (event) => {
        setCheckedAccount(event.target.checked);
    };

    const managelights = (listcompany) => {
        const itemlight = listcompany.filter(company => {
            return company.CompanyName == nameCompany
        })
        const DatabaseID = value.map((item) => {
            return item.DatabaseID
        })
        addDatabasePermission(itemlight[0].CompanyUID, DatabaseID)
    }

    const addDatabasePermission = async (CompanyUID, DatabaseID) => {
        const AccessToken = getCookie('AccessToken');
        const data = {
            "CompanyUID": CompanyUID,
            "DatabaseID": DatabaseID
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/addDatabasePermission'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log("Add Permission result ==> ", result)
        if (result.status == true) {
            console.log('Add Permission Pass', result)
            handleClickSubmit()

        } else {
            console.log('Add Permission Error', result)
        }
    }

    const handleClickSubmit = async () => {
        await getCompany()
        await getDatabasePermission()
        onClickBack("MainPageManageComp")
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const AccessToken = getCookie('AccessToken');
        let checkboxValue = ''
        if (checkedAccount == true && checkedSoftware == false) {
            checkboxValue = 'Account'
        } else if (checkedAccount == false && checkedSoftware == true) {
            checkboxValue = 'Software'
        } else if (checkedAccount == true && checkedSoftware == true) {
            checkboxValue = 'Account,Software'
        }
        const data = {
            CompanyName: nameCompany,
            CompanyType: checkboxValue,
            Remark: event.target.remark.value
        }
        const JSONdata = JSON.stringify(data)

        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company'

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }

        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log("Add company result ==> ", result)
        if (result.status == true) {
            console.log("Add company success!!")
            getDataCompany()
        }
    }

    const getDataCompany = async () => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company?DocStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            if (value.length !== 0) {
                managelights(result.data)
            } else {
                handleClickSubmit()
            }

        }
    }

    return (
        <Box>
            <AppBar
                position="static"
                sx={{ bgcolor: '#FFFFFF' }}
            >

                <Toolbar sx={{ alignContent: 'center', justifyContent: 'space-between' }}>

                    <IconButton
                        sx={{ border: 1, borderRadius: 1, borderColor: '#097FF5', color: '#097FF5' }}
                        onClick={() => handleClickSubmit()}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>เพิ่มบริษัท</Typography>
                    <IconButton sx={{ border: 1, borderRadius: 0, borderColor: '#fff', color: '#fff' }} >
                        <ArrowBackIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh', p: 2 }}>
                    <form onSubmit={handleSubmit}>
                        <Box>
                            <TextField
                                fullWidth
                                label="ชื่อบริษัท"
                                variant="outlined"
                                id="compname"
                                value={nameCompany}
                                onChange={(e) => setNameCompany(e.target.value)}
                                required
                            />
                        </Box>
                        <Box >
                            <FormControl component="fieldset" sx={{ pt: 2 }} required>
                                <FormGroup row sx={{ color: 'gray' }} >
                                    <FormControlLabel
                                        control={<Checkbox
                                            checked={checkedAccount}
                                            onChange={handleChangeAccount}
                                        />}
                                        label="บัญชี"
                                        labelPlacement="end"

                                    />
                                    <FormControlLabel
                                        control={<Checkbox
                                            checked={checkedSoftware}
                                            onChange={handleChangeSoftware}
                                        />}
                                        label="คอมพิวเตอร์"
                                        labelPlacement="end"
                                    />
                                </FormGroup>
                            </FormControl>
                        </Box>
                        <Box sx={{ py: 2 }}>
                            <Autocomplete
                                multiple
                                value={value}
                                onChange={(event, newValue) => {
                                    setValue(newValue);
                                }}
                                options={listDatabase}
                                getOptionLabel={(option) => option.DatabaseName}
                                filterSelectedOptions
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="เลือกฐานข้อมูล"
                                    />
                                )}
                            />
                        </Box>
                        <Box>
                            <TextField
                                fullWidth
                                label="หมายเหตุ"
                                multiline
                                rows={4}
                                id="remark"
                                value={remarkCompany}
                                onChange={(e) => setRemarkCompany(e.target.value)}
                            />
                        </Box>
                        <Box sx={{ pt: 5 }}>
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                type="submit"
                            >บันทึก</Button>
                        </Box>
                    </form>

                </Box>
            </Container>
        </Box>
    )
}
export default AddCompany;