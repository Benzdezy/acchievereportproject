import { Container } from "@mui/system";
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, FormGroup, FormControlLabel, FormControl, FormLabel, Stack, Autocomplete } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import Checkbox from '@mui/material/Checkbox';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState } from "react";
import Router from "next/router";
import { getCookie } from "cookies-next";


const EditCustomerA = (props) => {
    const { onClickBack, dataCustomerA, getCustomerA } = props;
    const router = Router;
    const [value, setValue] = useState([]);
    const [userID, setUserID] = useState(dataCustomerA.UserID)
    const [userName, setUserName] = useState(dataCustomerA.Name);
    const [userPhone, setUserPhone] = useState(dataCustomerA.Phone);
    const [userCompanyName, setUserCompanyName] = useState(dataCustomerA.CompanyName);
    const [userType, setUserType] = useState(dataCustomerA.UserType);

    const handleClickSubmit = () => {
        getCustomerA()
        onClickBack("MainPageManageRights")
    }

    const handleSubmit = async (event) => {
        // event.preventDefault()
        // console.log("databaseID",idDatabase)
        // const AccessToken = getCookie('AccessToken');
        // const data = {
        //     DatabaseName: event.target.databaseName.value,
        //     DatabasePort: event.target.databasePort.value,
        //     DatabaseHost: event.target.databaseHost.value,
        //     DatabaseUser: event.target.databaseUserName.value,
        //     DatabasePassword: event.target.databasePassword.value,
        // }
        // const JSONdata = JSON.stringify(data)

        // const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DatabaseID=${idDatabase}`

        // const options = {
        //     method: 'PUT',
        //     headers: {
        //         'Content-Type': 'application/json',
        //         Authorization: `Bearer ${AccessToken}`
        //     },
        //     body: JSONdata,
        // }

        // const response = await fetch(endpoint, options)

        // const result = await response.json()
        // console.log("result EditDatabase ==> ", result)
        // if (result.status == true) {
        //     handleClickSubmit()
        // }
    }


    return (
        <Box>
            <AppBar
                position="static"
                sx={{ bgcolor: '#FFFFFF' }}
            >

                <Toolbar sx={{ alignContent: 'center', justifyContent: 'space-between' }}>

                    <IconButton
                        sx={{ border: 1, borderRadius: 1, borderColor: '#097FF5', color: '#097FF5' }}
                        onClick={() => handleClickSubmit()}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>แก้ไขสิทธิ์ผู้ใช้งาน</Typography>
                    <IconButton sx={{ border: 1, borderRadius: 0, borderColor: '#fff', color: '#fff' }} >
                        <ArrowBackIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh', p: 2 }}>
                    <form onSubmit={handleSubmit}>
                        <Box>
                            <TextField
                                disabled
                                fullWidth
                                sx={{ mb: 2 }}
                                label="UserID"
                                variant="outlined"
                                type="text"
                                required
                                value={userID}
                                onChange={(e) => setUserID(e.target.value)}
                            />
                            <TextField
                                disabled
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Name"
                                variant="outlined"
                                type="text"
                                required
                                value={userName}
                                onChange={(e) => setUserName(e.target.value)}
                            />
                            <TextField
                                disabled
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Phone"
                                variant="outlined"
                                type="text"
                                required
                                value={userPhone}
                                onChange={(e) => setUserPhone(e.target.value)}
                            />
                            <TextField
                                disabled
                                fullWidth
                                sx={{ mb: 2 }}
                                label="Company Name"
                                variant="outlined"
                                type="text"
                                required
                                value={userCompanyName}
                                onChange={(e) => setUserCompanyName(e.target.value)}
                            />
                            <TextField
                                disabled
                                fullWidth
                                sx={{ mb: 2 }}
                                label="UserType"
                                variant="outlined"
                                type="text"
                                required
                                value={userType}
                                onChange={(e) => setUserType(e.target.value)}
                            />
                        </Box>
                        <Box sx={{ pt: 5 }}>
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                type="submit"

                            >บันทึก</Button>
                        </Box>
                    </form>
                </Box>
            </Container>
        </Box>
    )
}
export default EditCustomerA;