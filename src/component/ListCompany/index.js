import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Dialog,
    DialogContentText,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    List,
    Divider,
    styled,
    Box,
    Typography
} from "@mui/material";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from "react";
import { DataGrid } from '@mui/x-data-grid';
import ItemListCompany from "../ItemListCompany";
// import {useDemoData} from '@mui/x-data-grid-generator';


const ListCompany = ({ data, onClickEdit, getDataCompany, deleteDataCompany, getCompany, listDatabase, listDatabasePermission }) => {


    const [selectDelete, setSelectDelete] = useState();
    const [openDeleteAlert, setOpenDeleteAlert] = useState(false);


    // for (let i = 0; i < data.length; i++) {
    //     for (let j = 0; j < listDatabasePermission.length; j++) {
    //         if (listDatabasePermission[j].CompanyUID == data[i].CompanyUID) {
    //             const itemfilter = listDatabase.filter(database => {
    //                 return listDatabasePermission[j].DatabaseID == database.DatabaseID
    //             })
    //             console.log('itemfilter ==>> ',itemfilter)
    //             data[i].DatabaseID = listDatabasePermission[j].DatabaseID;
    //             // data[i].DatabaseName = itemfilter[0].DatabaseName;
    //         }
    //     }
    // }

    const newCompany = data.map(company => {
        if (company.DatabaseList.length == 0) {
            return company.DatabaseName = 'รอการเชื่อมต่อฐานข้อมูล'
        } else {
            return company
        }
    })

    const handleClickEdit = (list) => {
        onClickEdit("EditComp")
        getDataCompany(list)
    }

    const handleClickDelete = async () => {
        await deleteDataCompany(selectDelete.CompanyUID)
        await getCompany()
        setOpenDeleteAlert(false);
    }

    const handleOpenDeleteAlert = (list) => {
        setSelectDelete(list)
        setOpenDeleteAlert(true);
    };

    const handleCloseDeleteAlert = () => {
        setOpenDeleteAlert(false);
    };

    const StyledDialog = styled(Dialog)`
        .MuiBackdrop-root {
            background-color: #0000000a;
        },
        .MuiPaper-root {
            box-shadow: none
        }
    `;

    return (
        <div>
            <List>
                {data.map((list, index) => {
                    if (list.DatabaseList.length == 0) {
                        return (
                            <Box key={index} >
                                <ListItem sx={{ py: '10px' }}>
                                    <ListItemText
                                        primary={<Typography variant="h6">{list.CompanyName}</Typography>}
                                        secondary={<Typography variant="caption" sx={{ color: 'red' }}>{list.DatabaseName}</Typography>}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => handleClickEdit(list)}>
                                            <BorderColorIcon />
                                        </IconButton>
                                        <IconButton onClick={() => handleOpenDeleteAlert(list)}>
                                            <DeleteIcon />
                                        </IconButton>
                                        <StyledDialog
                                            open={openDeleteAlert}
                                            onClose={handleCloseDeleteAlert}
                                        >
                                            <DialogTitle>
                                                {`ผู้ใช้ต้องการลบข้อมูลบริษัท ${openDeleteAlert ? selectDelete.CompanyName : 'นี้'} หรือไม่`}
                                            </DialogTitle>
                                            <DialogActions>
                                                <Button onClick={handleCloseDeleteAlert}>ยกเลิก</Button>
                                                <Button onClick={handleClickDelete} autoFocus>
                                                    ยืนยัน
                                                </Button>
                                            </DialogActions>
                                        </StyledDialog>

                                    </ListItemSecondaryAction>
                                </ListItem>
                                <Divider />
                            </Box>
                        )
                    } else {
                        return (
                            <Box key={index} >
                                <ListItem sx={{ py: '10px' }}>
                                    <ListItemText
                                        primary={<Typography variant="h6">{list.CompanyName}</Typography>}
                                        secondary={<ItemListCompany data={list.DatabaseList} />}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton onClick={() => handleClickEdit(list)}>
                                            <BorderColorIcon />
                                        </IconButton>
                                        <IconButton onClick={() => handleOpenDeleteAlert(list)}>
                                            <DeleteIcon />
                                        </IconButton>
                                        <StyledDialog
                                            open={openDeleteAlert}
                                            onClose={handleCloseDeleteAlert}
                                        >
                                            <DialogTitle>
                                                {`ผู้ใช้ต้องการลบข้อมูลบริษัท ${openDeleteAlert ? selectDelete.CompanyName : 'นี้'} หรือไม่`}
                                            </DialogTitle>
                                            <DialogActions>
                                                <Button onClick={handleCloseDeleteAlert}>ยกเลิก</Button>
                                                <Button onClick={handleClickDelete} autoFocus>
                                                    ยืนยัน
                                                </Button>
                                            </DialogActions>
                                        </StyledDialog>

                                    </ListItemSecondaryAction>
                                </ListItem>
                                <Divider />
                            </Box>
                        )
                    }

                })}
            </List>

        </div>
    )
}
export default ListCompany;