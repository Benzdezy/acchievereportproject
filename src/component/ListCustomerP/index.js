import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Dialog,
    DialogContentText,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    List,
    Divider,
    styled,
    Box,
    Typography
} from "@mui/material";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from "react";

const ListCustomerP = (props) => {
    const { data, onClickEdit, setDataCustomerP } = props

    const [selectDelete, setSelectDelete] = useState();
    const [openDeleteAlert, setOpenDeleteAlert] = useState(false);

    const handleClickEdit = (list) => {
        onClickEdit("EditCustomerP")
        setDataCustomerP(list)
    }

    const handleOpenDeleteAlert = (list) => {
        setSelectDelete(list)
        setOpenDeleteAlert(true);
    };

    const handleCloseDeleteAlert = () => {
        setOpenDeleteAlert(false);
    };

    const handleClickDelete = async () => {
        await deleteDataCompany(selectDelete.CompanyUID)
        await getCompany()
        setOpenDeleteAlert(false);
    }

    const StyledDialog = styled(Dialog)`
        .MuiBackdrop-root {
            background-color: #0000000a;
        },
        .MuiPaper-root {
            box-shadow: none
        }
    `;

    return (
        <div>
            <List>
                {data.map((list, index) => {
                    return (
                        <Box key={index}>
                            <ListItem>
                                <ListItemText
                                    primary={<Typography variant="body1">{list.Name}</Typography>}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton onClick={() => handleClickEdit(list)}>
                                        <BorderColorIcon />
                                    </IconButton>
                                    {/* <IconButton>
                                        <DeleteIcon onClick={() => handleOpenDeleteAlert(list)} />
                                    </IconButton>
                                    <StyledDialog
                                        open={openDeleteAlert}
                                        onClose={handleCloseDeleteAlert}
                                    >
                                        <DialogTitle>
                                            {`ผู้ใช้ต้องการลบข้อมูลบริษัท ${openDeleteAlert ? selectDelete.CompanyName : 'นี้'} หรือไม่`}
                                        </DialogTitle>
                                        <DialogActions>
                                            <Button onClick={handleCloseDeleteAlert}>ยกเลิก</Button>
                                            <Button onClick={handleClickDelete} autoFocus>
                                                ยืนยัน
                                            </Button>
                                        </DialogActions>
                                    </StyledDialog> */}

                                </ListItemSecondaryAction>
                            </ListItem>
                            <Divider />
                        </Box>
                    )
                })}
            </List>
        </div>
    )
}
export default ListCustomerP