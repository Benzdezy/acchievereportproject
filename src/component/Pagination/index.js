import { Box, TablePagination, Pagination } from "@mui/material";
import { useEffect, useState } from "react";

const pageSize = 10;

const PaginationPage = (props) => {
    const { data, setNewListCompany } = props
    const [pagination, setPagination] = useState({
        count: 0,
        from: 0,
        to: pageSize
    })
    useEffect(() => {
        const list = data.slice(pagination.from, pagination.to);
        const lenList = data.length;
        setPagination({ ...pagination, count: lenList });
        setNewListCompany(list)
    }, [pagination.from, pagination.to])

    const handlePageChange = (event, page) => {
        const from = (page - 1) * pageSize;
        const to = (page - 1) * pageSize + pageSize;

        setPagination({ ...pagination, from: from, to: to })
    }

    return (
        <Box display={'flex'} justifyContent={"center"} alignContent="center" sx={{p: 1}}>
            <Pagination
                count={Math.ceil(pagination.count / pageSize)}
                onChange={handlePageChange}
                color="primary"
            />
        </Box>
    )
}
export default PaginationPage;