import { Container } from "@mui/system";
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, FormGroup, FormControlLabel, FormControl, FormLabel, Stack, Autocomplete } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import Checkbox from '@mui/material/Checkbox';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useState } from "react";
import Router from "next/router";
import { getCookie } from "cookies-next";


const AddDatabase = (props) => {
    const { onClickBack, data, getListDatabase } = props;
    const router = Router;
    const [value, setValue] = useState([]);

    const handleClickSubmit = () => {
        getListDatabase()
        onClickBack("MainPageManageDatabase")
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const AccessToken = getCookie('AccessToken');
        const data = {
            DatabaseName: event.target.databaseName.value,
            DatabaseNote: event.target.databaseNote.value,
            DatabasePort: event.target.databasePort.value,
            DatabaseHost: event.target.databaseHost.value,
            DatabaseUser: event.target.databaseUserName.value,
            DatabasePassword: event.target.databasePassword.value,
        }
        const JSONdata = JSON.stringify(data)
        
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database'

        const options = { 
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${AccessToken}`
            },
            body: JSONdata,
        }

        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log("result AddDatabase ==>> ",result)
        if (result.status == true) {
            console.log("Add database success!!")
            handleClickSubmit()
        }
    }

    return (
        <Box>
            <AppBar
                position="static"
                sx={{ bgcolor: '#FFFFFF' }}
            >

                <Toolbar sx={{ alignContent: 'center', justifyContent: 'space-between' }}>

                    <IconButton
                        sx={{ border: 1, borderRadius: 1, borderColor: '#097FF5', color: '#097FF5' }}
                        onClick={() => onClickBack("MainPageManageDatabase")}
                    >
                        <ArrowBackIcon />
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>เพิ่มฐานข้อมูล</Typography>
                    <IconButton sx={{ border: 1, borderRadius: 0, borderColor: '#fff', color: '#fff' }} >
                        <ArrowBackIcon />
                    </IconButton>
                </Toolbar>
            </AppBar>
            <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh', p: 2 }}>
                    <form onSubmit={handleSubmit}>
                        <Box>
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="Database name" 
                                variant="outlined"
                                type="text"
                                id="databaseName"
                                required
                            />
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="Database Note" 
                                variant="outlined"
                                type="text"
                                id="databaseNote"
                                required
                            />
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="Host" 
                                variant="outlined"
                                type="text"
                                id="databaseHost"
                                required
                            />
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="Port" 
                                variant="outlined"
                                type="text"
                                id="databasePort"
                                required
                            />
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="User name" 
                                variant="outlined"
                                type="text"
                                id="databaseUserName"
                                required
                            />
                            <TextField 
                                fullWidth
                                sx={{ mb: 2}}
                                label="Password" 
                                variant="outlined"
                                type="text"
                                id="databasePassword"
                                required
                            />
                        </Box>
                        <Box sx={{ pt: 5 }}>
                            <Button
                                fullWidth
                                variant="contained"
                                size="large"
                                type="submit"

                            >บันทึก</Button>
                        </Box>
                    </form>
                </Box>
            </Container>
        </Box>
    )
}
export default AddDatabase;