import { Box, Typography } from "@mui/material"

const ItemListCompany = (data) => {
    const listCompany = data.data
    // const itemDatabaseName = listCompany.map((itemListData, index) => {
    //     if (itemListData.DatabaseName == null) {
    //         return <Typography key={index} sx={{bgcolor: '#D4E9FE', px: '5px', m: '1px', color: 'red', borderRadius: '10px'}} variant="caption" component='div'>null</Typography>
    //     } else {
    //         return <Typography key={index} sx={{bgcolor: '#D4E9FE', px: '5px', m: '1px', borderRadius: '10px'}} variant="caption" component='div'>{itemListData.DatabaseName}</Typography>
    //     }
    // })
    // return itemDatabaseName
    return (
        <Box sx={{
            display: 'flex',
            width: '250px',
            flexWrap: 'wrap'
        }}>
            {listCompany.map((itemListData, index) => {
                if (itemListData.DatabaseName == null) {
                    return <Typography
                        key={index}
                        sx={{
                            bgcolor: '#D4E9FE',
                            px: '5px',
                            m: '1px',
                            color: 'red',
                            borderRadius: '10px'
                        }}
                        variant="caption"
                        component='div'
                    >
                        null
                    </Typography>
                } else {
                    return <Typography
                        key={index}
                        sx={{
                            bgcolor: '#D4E9FE',
                            px: '5px',
                            m: '1px',
                            borderRadius: '10px'
                        }}
                        variant="caption"
                        component='div'
                    >
                        {itemListData.DatabaseName}
                    </Typography>
                }
            })}
        </Box>
    )
}
export default ItemListCompany;