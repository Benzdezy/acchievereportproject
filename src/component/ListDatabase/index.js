import {
    IconButton,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Dialog,
    DialogContentText,
    DialogTitle,
    DialogContent,
    DialogActions,
    Button,
    List,
    Box,
    Divider,
    styled,
    Typography
} from "@mui/material";
import BorderColorIcon from '@mui/icons-material/BorderColor';
import DeleteIcon from '@mui/icons-material/Delete';
import { useState } from "react";

const ListDatabase = ({ data, onClickEdit, getDataDatabase, deleteDatabase, getListDatabase }) => {

    const [selectDelete, setSelectDelete] = useState();
    const [openDeleteAlert, setOpenDeleteAlert] = useState(false);

    const handleClickEdit = (list) => {
        onClickEdit("EditDatabase")
        getDataDatabase(list)
    }

    const handleClickDelete = async () => {
        console.log('item ==> ', selectDelete)
        await deleteDatabase(selectDelete.DatabaseID)
        await getListDatabase()
        setOpenDeleteAlert(false);
    }

    const handleOpenDeleteAlert = (list) => {
        console.log("item on click open == ", list)
        setSelectDelete(list)
        setOpenDeleteAlert(true);
    };

    const handleCloseDeleteAlert = () => {
        setOpenDeleteAlert(false);
    };

    const StyledDialog = styled(Dialog)`
        .MuiBackdrop-root {
            background-color: #0000000a;
        },
        .MuiPaper-root {
            box-shadow: none
        }
    `;

    return (
        <div>
            <List>
                {data.map((list, index) => {
                    return (
                        <Box key={index}>
                            <ListItem>
                                <ListItemText
                                    primary={<Typography variant="body1">{list.DatabaseName}</Typography>}
                                />
                                <ListItemSecondaryAction>
                                    <IconButton onClick={() => handleClickEdit(list)}>
                                        <BorderColorIcon />
                                    </IconButton>
                                    <IconButton onClick={() => handleOpenDeleteAlert(list)}>
                                        <DeleteIcon />
                                    </IconButton>
                                    <StyledDialog
                                        open={openDeleteAlert}
                                        onClose={handleCloseDeleteAlert}
                                    >
                                        <DialogTitle>
                                            {`ผู้ใช้ต้องการลบฐานข้อมูล ${openDeleteAlert ? selectDelete.DatabaseName : 'นี้'} หรือไม่`}
                                        </DialogTitle>
                                        <DialogActions>
                                            <Button onClick={handleCloseDeleteAlert}>ยกเลิก</Button>
                                            <Button onClick={handleClickDelete} autoFocus>
                                                ยืนยัน
                                            </Button>
                                        </DialogActions>
                                    </StyledDialog>

                                </ListItemSecondaryAction>
                            </ListItem>
                            <Divider />
                        </Box>
                    )
                })}
            </List>
        </div>
    )
}
export default ListDatabase;