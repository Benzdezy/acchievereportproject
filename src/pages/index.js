import Head from 'next/head'
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../styles/Home.module.css';

import { getCookie, setCookie } from 'cookies-next';


export default function Home() {

  const router = useRouter();
  const stringQuery = router.query;
  const value = Object.values(stringQuery)
  const text = value[0]
  if (text != undefined) {
    const arr = text.split("?path=")
    const path = arr[1]
    if (path == 'managecomp') {
      router.push('/managecomp')
    }
  }

  // useEffect(() => {
  //   import("@line/liff").then((liff) => {
  //     console.log("start liff.init()...");
  //     liff
  //       .init({
  //         liffId: '1657831817-dVZJ7NW8',
  //       })
  //       .then(() => {
  //         console.log("liff.init() done");
  //         // const stringLIFF_STORE = localStorage.getItem('LIFF_STORE:1657831817-dVZJ7NW8:context')
  //         // const jsonUID = JSON.parse(stringLIFF_STORE)
  //         // const userId = jsonUID.userId
  //         if (!liff.isInClient()) {
  //           if (liff.isLoggedIn()) {
  //             const profile = liff.getProfile();
  //             profile.then((result) => {
  //               setCookie('UserID', `${result.userId}`)
  //               onVerifyLogin(`${result.userId}`)
  //             });
  //             // onVerifyLogin(`${userId}`)
  //           } else {
  //             liff.login();
  //             console.log('กำลัง Login Home')
  //           }
  //         } else {
  //           const profile = liff.getProfile();
  //           profile.then((result) => {
  //             setCookie('UserID', `${result.userId}`)
  //             onVerifyLogin(`${result.userId}`)
  //           });
  //           // onVerifyLogin(`${userId}`)
  //         }
  //       })
  //       .catch((error) => {
  //         console.log(`liff.init() failed: ${error}`);
  //       });
  //   });
  // }, []);



  useEffect(() => {
    const userId = getCookie('UserID');

    // const stringLIFF_STORE = localStorage.getItem('LIFF_STORE:1657831817-dVZJ7NW8:context')
    // const jsonUID = JSON.parse(stringLIFF_STORE)
    // const userId = jsonUID.userId
    // onVerifyLogin(`${userId}`)
    getToken(`${userId}`)
  }, [])

  const getToken = async (userId) => {
    const data = {
      UserID: `${userId}`
    }
    const JSONdata = JSON.stringify(data)
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSONdata
    }
    const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/getToken'
    const response = await fetch(endpoint, options)
    const result = await response.json()
    if (result.status == false) {
      console.log("result.status false === ", result.status)
    }
    else if (result.status == true) {
      // const accessToken = result.data.AccessToken
      // const refreshToken = result.data.RefreshToken
      onVerifyLogin(result.data.AccessToken)

    }
  }

  const onVerifyLogin = async (cookies) => {
    const options = {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${cookies}`
      }
    }
    const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
    const response = await fetch(endpoint, options)
    const result = await response.json()
    if (result.status == false) {
      router.push('/login')
    } else if (result.status == true) {
      const dataUser = result.data.UserType
      console.log(dataUser)
      if (dataUser == "Customer") {
        console.log("Verify successfuly!! Customer")
        router.push('/viewreport')
      } else if (dataUser == "Admin") {
        console.log("Verify successfuly!! Admin")
        router.push('/managecomp')
      }
    }
  }

  return (
    <div>
      <Head>
        <title>LIFF Next App</title>
        <link rel="icon" href="/favicon.ico" />
        {/* <script src="https://unpkg.com/vconsole@latest/dist/vconsole.min.js"></script>
        <script>
          var vConsole = new window.VConsole();
        </script> */}
        {/* <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet"></link> */}
      </Head>
      <div >
        <div className={styles.container}><CircularProgress /></div>
      </div>
    </div>
  )
}