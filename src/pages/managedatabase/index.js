import { Container } from "@mui/system";
import { IconButton, InputAdornment, TextField, Toolbar, Divider, Autocomplete } from "@mui/material";
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import SearchIcon from '@mui/icons-material/Search';
import AddIcon from '@mui/icons-material/Add';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../../styles/Home.module.css'
import { useRouter } from 'next/router';
import { useEffect, useState } from "react";
import { deleteCookie, getCookie, setCookie } from "cookies-next";
import ListDatabase from "@/component/ListDatabase";
import AddDatabase from "@/component/AddDatabase";
import EditDatabase from "@/component/EditDatabase";

const ManageDatabase = () => {

    const router = useRouter();
    const [active, setActive] = useState("MainPageManageDatabase")
    const [alignment, setAlignment] = useState('ทั้งหมด');
    const [display, setDisplay] = useState(false);
    const [listDatabase, setListDatabase] = useState();
    const [statusDataDatabase, setStatusDataDatabase] = useState(false);
    const [dataDatabase, setDataDatabase] = useState();
    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        // const userId = getCookie('UserID');
        // const stringLIFF_STORE = localStorage.getItem('LIFF_STORE:1657831817-rzJgbMK6:context')
        // const jsonUID = JSON.parse(stringLIFF_STORE)
        // const userId = jsonUID.userId
        const accessToken = getCookie('AccessToken');
        const refreshToken = getCookie('RefreshToken');
        onVerifyProfile(accessToken, refreshToken)
    }, [])

    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status onVerifyProfile => ', result.status)
        if (result.status == false) {
            refreshToken(reToken)
        } else if (result.status == true) {
            const dataUser = result.data.UserType
            console.log(dataUser)
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/viewreport')
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                router.push('/managedatabase') //อาจไม่จำเป็น
                getDatabase(token)
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
            // router.push('/login')
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const getDatabase = async () => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DocStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
            // router.push('/login')
            // setStatusDataCompany(true)
        }
        else if (result.status == true) {
            const resultData = result.data
            const strAscending = [...resultData].sort((a, b) =>
                a.DatabaseName > b.DatabaseName ? 1 : -1,
            )
            setListDatabase(strAscending)
            setStatusDataDatabase(true)
        }
    }

    const deleteDatabase = async (DatabaseID) => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DatabaseID=${DatabaseID}`
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result false === ", result)
        }
        else if (result.status == true) {
            console.log("result true === ", result)
        }
    }

    const defaultProps = {
        options: listDatabase,
        getOptionLabel: (option) => option.DatabaseName,
    };

    return (
        <div>
            {active === "Loading" && <div className={styles.container}><CircularProgress /></div>}
            {active === "MainPageManageDatabase" &&
                <Box>
                    <AppBar
                        position="static"
                        sx={{ bgcolor: '#FFFFFF' }}
                    >
                        <Toolbar sx={{ justifyContent: 'center' }}>
                            <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>จัดการฐานข้อมูล</Typography>
                        </Toolbar>
                    </AppBar>
                    <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                        <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '85vh' }}>
                            {statusDataDatabase ?
                                <Autocomplete
                                    disablePortal
                                    inputValue={searchValue}
                                    onInputChange={(event, newValue) => {
                                        setSearchValue(newValue)
                                    }}
                                    {...defaultProps}
                                    sx={{
                                        width: '100%',
                                        '& .MuiInputBase-root': { padding: 0, paddingRight: 0 },
                                        '& .MuiOutlinedInput-root': { paddingRight: '0px !important', paddingLeft: 2 },
                                    }}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            sx={{
                                                p: 2,
                                                width: '100%',
                                                '& .MuiOutlinedInput-root': { paddingRight: 0 },
                                                '& .MuiIconButton-root': { p: '16px', borderRadius: 0, backgroundColor: '#0080FF', color: '#ffffff', borderColor: '#0080FF' },
                                                '& .MuiIconButton-root.Mui-selected': { p: '16px', borderRadius: 0, backgroundColor: '#0080FF', color: '#ffffff', borderColor: '#0080FF' },


                                            }}
                                            placeholder="ค้นหาชื่อฐานข้อมูล..."

                                            InputProps={{
                                                ...params.InputProps,
                                                startAdornment: (
                                                    <>
                                                        <InputAdornment position="start">
                                                            <SearchIcon />
                                                        </InputAdornment>
                                                        {params.InputProps.startAdornment}
                                                    </>
                                                ),
                                                endAdornment: (
                                                    <>
                                                        <IconButton position="end" onClick={() => setActive("AddDatabase")}>
                                                            <AddIcon />
                                                        </IconButton>
                                                    </>
                                                )
                                            }}
                                        />
                                    }
                                />
                                : null}
                            <Divider />
                            <Box sx={{ px: 2, pt: 2, justifyContent: 'space-between', alignContent: 'center', display: 'flex' }}>
                                <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                    ชี่อ/ฐานข้อมูล
                                </Typography>
                                <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                    แก้ไข/ลบ
                                </Typography>
                            </Box>
                            <Box sx={{ overflow: 'auto', height: '68vh' }}>
                                {statusDataDatabase
                                    ? <ListDatabase data={listDatabase.filter((database) => {
                                        return database.DatabaseName.includes(searchValue)
                                    })} onClickEdit={setActive}
                                        getDataDatabase={setDataDatabase}
                                        deleteDatabase={deleteDatabase}
                                        getListDatabase={getDatabase} />
                                    : null}

                            </Box>
                        </Box>
                    </Container>
                </Box>
            }
            {active === "AddDatabase" && <AddDatabase onClickBack={setActive} getListDatabase={getDatabase} />}
            {active === "EditDatabase" && <EditDatabase onClickBack={setActive} data={dataDatabase} getListDatabase={getDatabase} />}
        </div>
    )
}
export default ManageDatabase;