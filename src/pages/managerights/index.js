import { Container } from "@mui/system";
import Paper from '@mui/material/Paper';
import { AppBar, Autocomplete, Box, Divider, IconButton, InputAdornment, Tab, Tabs, TextField, Toolbar, Typography } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { useEffect, useState } from "react";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { deleteCookie, getCookie, setCookie } from "cookies-next";
import { useRouter } from 'next/router';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../../styles/Home.module.css'
import ListCustomerP from "@/component/ListCustomerP";
import ListCustomerA from "@/component/ListCustomerA";
import EditCustomer from "@/component/EditCustomerA";
import EditCustomerA from "@/component/EditCustomerA";
import EditCustomerP from "@/component/EditCustomerP";

const TabPanel = (props) => {
    const { children, value, index } = props;
    return (
        <div
            hidden={value !== index}
        >
            {value === index &&
                <Box>
                    {children}
                </Box>
            }
        </div>
    )
}

const ManageRights = () => {

    const router = useRouter();
    const [active, setActive] = useState("Loading")
    // const [active, setActive] = useState("MainPageManagerights") //ระหว่างที่ Server ล่ม
    const [value, setValue] = useState(1)
    const [alignment, setAlignment] = useState('All');
    const [searchValueCustomerP, setSearchValueCustomerP] = useState('');
    const [searchValueCustomerA, setSearchValueCustomerA] = useState('');
    const [dataCustomerA, setDataCustomerA] = useState();
    const [dataCustomerP, setDataCustomerP] = useState();
    const [listCustomerA, setListCustomerA] = useState();
    const [listCustomerP, setListCustomerP] = useState();
    const [statusDataCustomerA, setStatusDataCustomerA] = useState(false);
    const [statusDataCustomerP, setStatusDataCustomerP] = useState(false);

    const handleTabs = (e, val) => {
        setValue(val)
    }

    useEffect(() => {
        // const userId = getCookie('UserID');
        const accessToken = getCookie('AccessToken');
        const refreshToken = getCookie('RefreshToken');
        onVerifyProfile(accessToken, refreshToken)
    }, [])

    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status onVerifyProfile => ', result.status)
        if (result.status == false) {
            refreshToken(reToken)
        } else if (result.status == true) {
            const dataUser = result.data.UserType
            console.log(dataUser)
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/viewreport')
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                router.push('/managerights') //อาจไม่จำเป็น
                setActive('MainPageManageRights')
                getCustomerA()
                getCustomerP()
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
            router.push('/login')
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const getCustomerA = async () => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/customer?UserStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            const resultData = result.data
            setListCustomerA(resultData)
            setStatusDataCustomerA(true)
        }
    }

    const getCustomerP = async () => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/customer?UserStatus=P'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            const resultData = result.data
            setListCustomerP(resultData)
            setStatusDataCustomerP(true)
        }
    }

    const handleChangeType = (event, newAlignment) => {
        if (newAlignment !== null) {
            setAlignment(newAlignment);
        }
        // else if (newAlignment == "All") {
        //     setAlignment(newAlignment);
        // }
    };

    const defaultPropsP = {
        options: listCustomerP,
        getOptionLabel: (option) => option.Name,
    };

    const defaultPropsA = {
        options: listCustomerA,
        getOptionLabel: (option) => option.Name,
    };

    return (
        <div>
            {active === "Loading" && <div className={styles.container}><CircularProgress /></div>}
            {active === "MainPageManageRights" &&
                <Box>
                    <AppBar
                        position="static"
                        sx={{ bgcolor: '#FFFFFF' }}
                    >
                        <Tabs
                            variant="fullWidth"
                            value={value}
                            onChange={handleTabs}
                            sx={{ '& .MuiTab-root': { fontSize: '0.975rem' } }}
                        >
                            <Tab label="รอดำเนินการ" />
                            <Tab label="ทั้งหมด" />
                        </Tabs>
                    </AppBar>
                    <TabPanel value={value} index={0}>
                        <Container
                            maxWidth="lg"
                            sx={{ justifyContent: 'center' }}
                        >
                            <Box
                                sx={{
                                    boxShadow: 3,
                                    mt: 2,
                                    borderRadius: 2,
                                    height: '85vh'
                                }}
                            >
                                {statusDataCustomerP ?
                                    <Autocomplete
                                        freeSolo
                                        disableClearable
                                        inputValue={searchValueCustomerP}
                                        onInputChange={(event, newValue) => {
                                            setSearchValueCustomerP(newValue)
                                        }}
                                        {...defaultPropsP}
                                        sx={{
                                            '& .MuiTextField-root': { p: '16px' },
                                            '& .MuiOutlinedInput-root': { p: '0px' },
                                            '& .MuiInputAdornment-root': { ml: 1 }
                                        }}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                placeholder="ค้นหา..."
                                                InputProps={{
                                                    ...params.InputProps,
                                                    type: 'search',
                                                    startAdornment: (
                                                        <>
                                                            <InputAdornment position="start">
                                                                <SearchIcon />
                                                            </InputAdornment>
                                                            {params.InputProps.startAdornment}
                                                        </>
                                                    ),
                                                }}
                                            />
                                        )}
                                    />
                                    : null
                                }
                                <Divider />
                                <Box sx={{ px: 2, pt: 2, justifyContent: 'space-between', alignContent: 'center', display: 'flex' }}>
                                    <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                        บริษัท/ฐานข้อมูล
                                    </Typography>
                                    <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                        แก้ไข/ลบ
                                    </Typography>
                                </Box>
                                <Box sx={{ overflow: 'auto', height: '57vh' }}>
                                    {statusDataCustomerP
                                        ? <ListCustomerP
                                            data={listCustomerP.filter((customer) => {
                                                return customer.Name.includes(searchValueCustomerP)
                                            })}
                                            onClickEdit={setActive}
                                            setDataCustomerP={setDataCustomerP}
                                            getCustomerP={getCustomerP}
                                        // getDataCompany={setDataCompany}
                                        // deleteDataCompany={deleteCompany}
                                        // getCompany={getCompany}
                                        // listDatabase={listDatabase}
                                        // listDatabasePermission={listDatabasePermission}
                                        // getDatabasePermission={getDatabasePermission}
                                        />
                                        : null
                                    }
                                </Box>
                            </Box>

                        </Container>
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <Container
                            maxWidth="lg"
                            sx={{ justifyContent: 'center' }}
                        >
                            <Box
                                sx={{
                                    boxShadow: 3,
                                    mt: 2,
                                    borderRadius: 2,
                                    height: '85vh'
                                }}
                            >
                                {statusDataCustomerP ?
                                    <Autocomplete
                                        sx={{
                                            '& .MuiTextField-root': { p: '16px' },
                                            '& .MuiOutlinedInput-root': { p: '0px' },
                                            '& .MuiInputAdornment-root': { ml: 1 }
                                        }}
                                        freeSolo
                                        disableClearable
                                        inputValue={searchValueCustomerA}
                                        onInputChange={(event, newValue) => {
                                            setSearchValueCustomerA(newValue)
                                        }}
                                        {...defaultPropsA}
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                placeholder="ค้นหา..."
                                                InputProps={{
                                                    ...params.InputProps,
                                                    type: 'search',
                                                    startAdornment: (
                                                        <>
                                                            <InputAdornment position="start">
                                                                <SearchIcon />
                                                            </InputAdornment>
                                                            {params.InputProps.startAdornment}
                                                        </>
                                                    )
                                                }}
                                            />
                                        )}
                                    />
                                    : null
                                }
                                <Box sx={{ px: 2, justifyContent: 'center', display: "flex" }}>
                                    <ToggleButtonGroup
                                        color="primary"
                                        exclusive
                                        fullWidth
                                        size="small"
                                        value={alignment}
                                        onChange={handleChangeType}
                                        sx={{
                                            '& .MuiToggleButton-root.Mui-selected': {
                                                color: '#ffffff', backgroundColor: '#097FF5'
                                            },
                                            '& .MuiToggleButton-root.Mui-selected:hover': {
                                                color: '#ffffff', backgroundColor: '#097FF5'
                                            },
                                            '& .MuiToggleButton-root': {
                                                borderColor: '#0080FF', color: '#0080FF'
                                            },
                                            height: '30px'
                                        }}
                                    >
                                        <ToggleButton value="Customer">ลูกค้า</ToggleButton>
                                        <ToggleButton value="Admin">แอดมิน</ToggleButton>
                                        <ToggleButton value="All">ทั้งหมด</ToggleButton>
                                    </ToggleButtonGroup>
                                </Box>
                                <Box sx={{ px: 2, pt: 2, justifyContent: 'space-between', alignContent: 'center', display: 'flex' }}>
                                    <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                        บริษัท/ฐานข้อมูล
                                    </Typography>
                                    <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                        แก้ไข/ลบ
                                    </Typography>
                                </Box>
                                <Box sx={{ overflow: 'auto', height: '57vh' }}>
                                    {statusDataCustomerA
                                        ? <ListCustomerA
                                            data={listCustomerA.filter((customer) => {
                                                if (alignment == 'All') {
                                                    return customer.Name.includes(searchValueCustomerA)
                                                } else {
                                                    return customer.Name.includes(searchValueCustomerA) && customer.UserType.includes(alignment)
                                                }

                                            })}
                                            onClickEdit={setActive}
                                            setDataCustomerA={setDataCustomerA}
                                            getCustomerA={getCustomerA}
                                        // deleteDataCompany={deleteCompany}
                                        // listDatabase={listDatabase}
                                        // listDatabasePermission={listDatabasePermission}
                                        // getDatabasePermission={getDatabasePermission}
                                        />
                                        : null
                                    }
                                </Box>
                            </Box>
                        </Container>
                    </TabPanel>
                </Box>
            }
            {active === "EditCustomerA" && <EditCustomerA
                onClickBack={setActive}
                dataCustomerA={dataCustomerA}
                getCustomerA={getCustomerA}
            />}
            {active === "EditCustomerP" && <EditCustomerP
                onClickBack={setActive}
                dataCustomerP={dataCustomerP}
                getCustomerP={getCustomerP}
            />}
        </div>
    )
}
export default ManageRights;