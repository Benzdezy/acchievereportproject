import { Container } from "@mui/system";
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Paper from '@mui/material/Paper';
import { ButtonGroup, Card, Divider, IconButton, InputAdornment, TextField, Toolbar, Button, List, ListItem, ListItemText, Chip, Stack, Autocomplete, Alert } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import { deleteCookie, getCookie, setCookie } from "cookies-next";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../../styles/Home.module.css'
import Box from '@mui/material/Box';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import AddIcon from '@mui/icons-material/Add';
import AddCompany from "@/component/Addcomp";
import ListCompany from "@/component/ListCompany";
import EditCompany from "@/component/EditComp";
import Pagination from "@/component/Pagination";
import PaginationPage from "@/component/Pagination";



const ManageComp = () => {

    const router = useRouter();
    const [active, setActive] = useState("Loading")
    const [alignment, setAlignment] = useState('All');
    const [display, setDisplay] = useState(false);
    const [listCompany, setListCompany] = useState();
    const [statusDataCompany, setStatusDataCompany] = useState(false);
    const [getAccessToken, setGetAccessToken] = useState('')
    const [dataCompany, setDataCompany] = useState();
    const [searchValue, setSearchValue] = useState('');
    const [listDatabase, setListDatabase] = useState();
    const [statusDataDatabase, setStatusDataDatabase] = useState(false);
    const [listDatabasePermission, setListDatabasePermission] = useState();
    const [newListCompany, setNewListCompany] = useState();

    const [statusDataDatabasePermission, setStatusDataDatabasePermission] = useState(false);
    useEffect(() => {
        // const userId = getCookie('UserID');
        // getToken(`${userId}`)
        const accessToken = getCookie('AccessToken');
        const refreshToken = getCookie('RefreshToken');
        onVerifyProfile(accessToken, refreshToken)
    }, [])

    // const getToken = async (userId) => {
    //     const data = {
    //         UserID: `${userId}`
    //     }
    //     const JSONdata = JSON.stringify(data)
    //     const options = {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSONdata
    //     }
    //     const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/getToken'
    //     const response = await fetch(endpoint, options)
    //     const result = await response.json()
    //     if (result.status == false) {
    //         console.log("result.status false === ", result.status)
    //     }
    //     else if (result.status == true) {
    //         // const accessToken = result.data.AccessToken
    //         // const refreshToken = result.data.RefreshToken
    //         setGetAccessToken(result.data.AccessToken)
    //         onVerifyLogin(result.data.AccessToken)

    //     }
    // }


    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status onVerifyProfile => ', result.status)
        if (result.status == false) {
            refreshToken(reToken)
        } else if (result.status == true) {
            const dataUser = result.data.UserType
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/viewreport')
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                router.push('/managecomp') //อาจไม่จำเป็น
                getCompany()
                getDatabase()
                getDatabasePermission()
                setActive('MainPageManageComp')
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
            router.push('/login')
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const getCompany = async () => {
        setStatusDataCompany(false)
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company?DocStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            const resultData = result.data
            const strAscending = [...resultData].sort((a, b) =>
                a.CompanyName > b.CompanyName ? 1 : -1,
            )
            setListCompany(strAscending)
            setStatusDataCompany(true)
        }
    }

    const getDatabase = async () => {
        setStatusDataDatabase(false)
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/database?DocStatus=A'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            const resultData = result.data
            const strAscending = [...resultData].sort((a, b) =>
                a.DatabaseName > b.DatabaseName ? 1 : -1,
            )
            setListDatabase(strAscending)
            setStatusDataDatabase(true)
        }
    }

    const getDatabasePermission = async () => {
        setStatusDataDatabasePermission(false)
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/getDatabasePermission'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result.status === ", result)
        }
        else if (result.status == true) {
            setListDatabasePermission(result.data)
            setStatusDataDatabasePermission(true)
        }
    }

    const deleteCompany = async (CompanyUID) => {
        const accessToken = getCookie('AccessToken');
        const options = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${accessToken}`
            }
        }
        const endpoint = `https://api.lineoa.acchieve.com/acchiveReport/api/v1/manage/company?CompanyUID=${CompanyUID}`
        const response = await fetch(endpoint, options)
        const result = await response.json()
        if (result.status == false) {
            console.log("result false === ", result)
        }
        else if (result.status == true) {
            console.log("result true === ", result)
        }
    }

    const defaultProps = {
        options: listCompany,
        getOptionLabel: (option) => option.CompanyName,
    };


    const handleChangeType = (event, newAlignment) => {
        if (newAlignment !== null) {
            setAlignment(newAlignment);
        }
    };

    const handleEditCompDeletePermission = async() => {
        await setActive('Loading')
        await getCompany()
        await getDatabasePermission()
        await setActive('EditComp')

    }


    return (
        <div>
            {active === "Loading" && <div className={styles.container}><CircularProgress /></div>}
            {active === "MainPageManageComp" &&
                <Box>
                    <AppBar
                        position="static"
                        sx={{ bgcolor: '#FFFFFF' }}
                    >
                        <Toolbar sx={{ justifyContent: 'center' }}>
                            <Typography variant="h6" component="div" sx={{ color: '#097FF5' }}>จัดการบริษัท</Typography>
                        </Toolbar>
                    </AppBar>
                    <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                        <Box sx={{ boxShadow: 3, mt: 2, borderRadius: 2, height: '89vh' }}>
                            {statusDataCompany && statusDataDatabase && statusDataDatabasePermission
                                ?
                                <Autocomplete
                                    disablePortal
                                    inputValue={searchValue}
                                    onInputChange={(event, newValue) => {
                                        setSearchValue(newValue)
                                    }}
                                    {...defaultProps}
                                    sx={{
                                        width: '100%',
                                        '& .MuiInputBase-root': { padding: 0, paddingRight: 0 },
                                        '& .MuiOutlinedInput-root': { paddingRight: '0px !important', paddingLeft: 2 },
                                    }}
                                    renderInput={(params) =>
                                        <TextField
                                            {...params}
                                            sx={{
                                                p: 2,
                                                width: '100%',
                                                '& .MuiOutlinedInput-root': { paddingRight: 0 },
                                                '& .MuiIconButton-root': { p: '16px', borderRadius: 0, backgroundColor: '#0080FF', color: '#ffffff', borderColor: '#0080FF' },
                                                '& .MuiIconButton-root.Mui-selected': { p: '16px', borderRadius: 0, backgroundColor: '#0080FF', color: '#ffffff', borderColor: '#0080FF' },


                                            }}
                                            placeholder="ค้นหาชื่อบริษัท..."
                                            InputProps={{
                                                ...params.InputProps,
                                                startAdornment: (
                                                    <>
                                                        <InputAdornment position="start">
                                                            <SearchIcon />
                                                        </InputAdornment>
                                                        {params.InputProps.startAdornment}
                                                    </>
                                                ),
                                                endAdornment: (
                                                    <>
                                                        <IconButton position="end" onClick={() => setActive("AddComp")}>
                                                            <AddIcon />
                                                        </IconButton>
                                                    </>
                                                )
                                            }}
                                        />
                                    }
                                />
                                : <div className={styles.container}><CircularProgress /></div>}
                            <Divider />
                            <Box sx={{ p: 2, justifyContent: 'center', display: "flex" }}>
                                <ToggleButtonGroup
                                    color="primary"
                                    exclusive
                                    fullWidth
                                    value={alignment}
                                    onChange={handleChangeType}
                                    sx={{
                                        '& .MuiToggleButton-root.Mui-selected': {
                                            color: '#ffffff', backgroundColor: '#097FF5'
                                        },
                                        '& .MuiToggleButton-root.Mui-selected:hover': {
                                            color: '#ffffff', backgroundColor: '#097FF5'
                                        },
                                        '& .MuiToggleButton-root': {
                                            borderColor: '#0080FF', color: '#0080FF'
                                        },
                                    }}
                                >
                                    <ToggleButton value="Account" onClick={() => {
                                        getCompany()
                                        getDatabase()
                                        getDatabasePermission()
                                    }}>บัญชี</ToggleButton>
                                    <ToggleButton value="Software" onClick={() => {
                                        getCompany()
                                        getDatabase()
                                        getDatabasePermission()
                                    }}>คอมพิวเตอร์</ToggleButton>
                                    <ToggleButton value="All" onClick={() => {
                                        getCompany()
                                        getDatabase()
                                        getDatabasePermission()
                                    }}>ทั้งหมด</ToggleButton>
                                </ToggleButtonGroup>
                            </Box>
                            <Box sx={{ px: 2, justifyContent: 'space-between', alignContent: 'center', display: 'flex' }}>
                                <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                    ชี่อ/บริษัท
                                </Typography>
                                <Typography component="span" variant="subtitle2" sx={{ color: '#097FF5' }}>
                                    แก้ไข/ลบ
                                </Typography>
                            </Box>
                            <Box sx={{ overflow: 'auto', height: '57vh' }}>
                                {/* {statusDataCompany && statusDataDatabase && statusDataDatabasePermission
                                    ? <ListCompany data={listCompany.filter((company) => {
                                        if (alignment == "All") {
                                            return company.CompanyName.includes(searchValue)
                                        } else {
                                            return company.CompanyName.includes(searchValue) && company.CompanyType.includes(alignment)
                                        }
                                    })} onClickEdit={setActive}
                                        getDataCompany={setDataCompany}
                                        deleteDataCompany={deleteCompany}
                                        getCompany={getCompany}
                                        listDatabase={listDatabase}
                                        listDatabasePermission={listDatabasePermission}
                                        getDatabasePermission={getDatabasePermission}
                                    />
                                    : null
                                } */}

                                {newListCompany != undefined
                                    ? <ListCompany
                                        data={newListCompany.filter((company) => {
                                            if (alignment == "All") {
                                                return company.CompanyName.includes(searchValue)
                                            } else {
                                                return company.CompanyName.includes(searchValue) && company.CompanyType.includes(alignment)
                                            }
                                        })}
                                        onClickEdit={setActive}
                                        getDataCompany={setDataCompany}
                                        deleteDataCompany={deleteCompany}
                                        getCompany={getCompany}
                                        listDatabase={listDatabase}
                                        listDatabasePermission={listDatabasePermission}
                                        getDatabasePermission={getDatabasePermission}
                                    />
                                    : null
                                }
                            </Box>
                            {statusDataCompany && statusDataDatabase && statusDataDatabasePermission
                                ? <PaginationPage data={listCompany.filter((company) => {
                                    if (alignment == "All") {
                                        return company.CompanyName.includes(searchValue)
                                    } else {
                                        return company.CompanyName.includes(searchValue) && company.CompanyType.includes(alignment)
                                    }
                                })}
                                    setNewListCompany={setNewListCompany}
                                />
                                : null
                            }
                        </Box>
                    </Container>
                </Box>
            }
            {active === "AddComp" && <AddCompany
                onClickBack={setActive}
                getCompany={getCompany}
                getDatabasePermission={getDatabasePermission}
                listDatabase={listDatabase}
            />}
            {active === "EditComp" && <EditCompany
                onClickBack={setActive}
                data={dataCompany}
                getCompany={getCompany}
                getDatabasePermission={getDatabasePermission}
                listDatabase={listDatabase}
                listDatabasePermission={listDatabasePermission}
                handleEditCompDeletePermission={handleEditCompDeletePermission}
            />}
        </div>
    )
}
export default ManageComp;