import { useEffect, useState } from 'react'
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import styles from '../../styles/Home.module.css';
import CircularProgress from '@mui/material/CircularProgress';
import Card from '@mui/material/Card';
import { FormControl } from '@mui/material';
import { useRouter } from 'next/router';

import { setCookie, getCookie, deleteCookie } from 'cookies-next';


const Login = () => {

    const [liffObject, setLiffObject] = useState(null);
    const [display, setDisplay] = useState(false);

    const router = useRouter()

    useEffect(() => {
        import("@line/liff").then((liff) => {
            console.log("start liff.init()...");
            liff
                .init({
                    liffId: '1657831817-ZXG4Q5v8',
                })
                .then(() => {
                    console.log("liff.init() done");
                    // setLiffObject(liff);
                    // console.log('liff', liff)
                    if (!liff.isInClient()) {
                        if (liff.isLoggedIn()) {
                            const profile = liff.getProfile();
                            profile.then((result) => {
                                setCookie('UserID', `${result.userId}`)
                                getToken(result.userId, liff)
                                //router.push('/testpage')
                            });
                            // setDisplay(true);
                        } else {
                            liff.login();
                            console.log('กำลัง Login login page')
                        }
                    } else {

                        const profile = liff.getProfile();
                        profile.then((result) => {
                            setCookie('UserID', `${result.userId}`)
                            getToken(result.userId, liff)
                        });
                        // setDisplay(true);
                    }
                })
                .catch((error) => {
                    console.log(`liff.init() failed: ${error}`);
                    // setLiffError(error.toString());
                });
        });
    }, []);

    const getToken = async (userId, liff) => {

        const data = {
            // UserID: `${userId}`
            UserID: `U69d7f7e557c6941ed7a4211406c3c274`
        }
        const JSONdata = JSON.stringify(data)
        console.log('JSONdata => ', JSONdata)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/getToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('result getToken == ', result)
        if (result.status == false) {
            if (result.message == 'ไม่พบ UID ดังกล่าวในระบบ') {
                setDisplay(true);
            } else if (result.message == 'UID ไม่พร้อมใช้งาน') {
                // แจ้งเตือนบอก ผู้ใช้
                alert("รอการอนุมัติจาก Admin")
                liff.closeWindow()
            }
        }
        else if (result.status == true) {
            setCookie('AccessToken', `${result.data.AccessToken}`)
            setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)

        }
    }

    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('onVerifyProfile => ', result)
        if (result.status == false) {
            refreshToken(reToken)

        } else if (result.status == true) {
            const dataUser = result.data.UserType
            console.log(dataUser)
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/viewreport')
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                // router.push('/managecomp')
                router.push('/managedatabase')
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
            setDisplay(true);
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const UserID = getCookie('UserID');
        const data = {
            UserID: `${UserID}`,
            Name: event.target.userName.value,
            Phone: event.target.phone.value,
            CompanyName: event.target.companyName.value,
        }
        const JSONdata = JSON.stringify(data)

        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/register'

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata,
        }

        const response = await fetch(endpoint, options)

        const result = await response.json()
        if (result.status == true) {
            // router.push('/')
            await getToken(UserID)
        }
    }

    return (
        <div className={styles.container}>
            {display
                ? <Card sx={{ minWidth: 275 }}>
                    <div className={styles.main}>
                        <h1>ลงทะเบียน</h1>
                        <form onSubmit={handleSubmit}>
                            <Box sx={{ width: '300px' }}>
                                <TextField sx={{ mb: 2, width: '100%' }} id="userName" label="ชื่อผู้ใช้งาน" variant="outlined" type="text" />
                                <TextField sx={{ mb: 2, width: '100%' }} id="phone" label="เบอร์โทรศัพท์" variant="outlined" type="number" />
                                <TextField sx={{ mb: 2, width: '100%' }} id="companyName" label="ชื่อบริษัท" variant="outlined" type="text" />
                                <Button sx={{ width: '100%' }} size="large" variant="contained" type='submit'>ลงทะเบียน</Button>
                            </Box>
                        </form>
                    </div>
                </Card>
                : <CircularProgress />}
        </div>
        // <div className={styles.container}>
        //     <Card sx={{ minWidth: 275 }}>
        //         <div className={styles.main}>
        //             <h1>ลงทะเบียน</h1>
        //             <form onSubmit={handleSubmit}>
        //                 <Box sx={{ width: '300px' }}>
        //                     <TextField sx={{ mb: 2, width: '100%' }} id="userName" label="ชื่อผู้ใช้งาน" variant="outlined" onChange={(e) => setUserNameInput(e.target.value)} type="text" />
        //                     <TextField sx={{ mb: 2, width: '100%' }} id="phone" label="เบอร์โทรศัพท์" variant="outlined" onChange={(e) => setPasswordInput(e.target.value)} type="number" />
        //                     <TextField sx={{ mb: 2, width: '100%' }} id="companyName" label="ชื่อบริษัท" variant="outlined" onChange={(e) => setUserNameInput(e.target.value)} type="text" />
        //                     <Button sx={{ width: '100%' }} size="large" variant="contained" type='submit'>ลงทะเบียน</Button>
        //                 </Box>
        //             </form>
        //         </div>
        //     </Card>
        // </div>
    )
}
export default Login;