import { Container } from "@mui/system";
import Paper from '@mui/material/Paper';
import { Box, IconButton, TextField, Typography, InputAdornment, makeStyles } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { getCookie, deleteCookie, setCookie } from "cookies-next";
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../../styles/Home.module.css'
import Button from '@mui/material/Button';
import { AccountCircle } from "@mui/icons-material";

const viewReport = () => {

    const router = useRouter();
    const [display, setDisplay] = useState(false);

    useEffect(() => {
        // const userId = getCookie('UserID');
        const accessToken = getCookie('AccessToken');
        const refreshToken = getCookie('RefreshToken');
        // const stringLIFF_STORE = localStorage.getItem('LIFF_STORE:1657831817-rzJgbMK6:context')
        // const jsonUID = JSON.parse(stringLIFF_STORE)
        // const userId = jsonUID.userId
        // getToken(`${userId}`)
        onVerifyProfile(accessToken, refreshToken)
    }, [])

    // const getToken = async (userId) => {
    //     const data = {
    //         UserID: `${userId}`
    //     }
    //     const JSONdata = JSON.stringify(data)
    //     const options = {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSONdata
    //     }
    //     const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/getToken'
    //     const response = await fetch(endpoint, options)
    //     const result = await response.json()
    //     if (result.status == false) {
    //         console.log("result.status false === ", result.status)
    //     }
    //     else if (result.status == true) {
    //         // const accessToken = result.data.AccessToken
    //         // const refreshToken = result.data.RefreshToken
    //         onVerifyProfile(result.data.AccessToken)

    //     }
    // }

    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status onVerifyProfile => ', result.status)
        if (result.status == false) {
            refreshToken(reToken)
        } else if (result.status == true) {
            const dataUser = result.data.UserType
            console.log(dataUser)
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/viewreport') //อาจไม่จำเป็น
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                router.push('/viewreport') //อาจไม่จำเป็น
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const handleLogout = async (cookies) => {
        const options = {
            method: 'DELETE',
            headers: {
                Authorization: `${cookies}`
            }
        }
        console.log('options :: ', options)
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/logout'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('result log out === ', result)
        if (result.status == false) {
            router.push('/')
        } else {
            deleteCookie('UserID')
            console.log("Logout successfuly!!")
            router.push('/login')
        }
    }

    const onClickLogOut = async () => {
        const cookies = getCookie('UserID')
        handleLogout(cookies)
    }
    const handlechange = (e) => {
        e.preventDefault()
        console.log("value == ", e)
    }

    return (

        <Box>
            {display
                ? <Container maxWidth="lg">

                    <Paper
                        elevation={3}
                        component="form"
                        sx={{ justifyContent: 'center', mt: 2, p: 2, height: '95vh' }}
                    >
                        <TextField
                            placeholder="ค้นหา..."
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <SearchIcon />
                                    </InputAdornment>
                                ),

                            }}
                            fullWidth

                        />
                        <Box sx={{ display: 'flex', mt: 2 }}>
                            <Typography variant="h4" sx={{mr: 2}}>ดูรายงาน</Typography>
                            <Button variant="contained" size="medium" onClick={() => onClickLogOut()}>ออกจากระบบ</Button>
                        </Box>


                    </Paper>
                    {/* <Typography>
                        หน้าทดสอบ input
                    </Typography>
                    <TextField>

                    </TextField> */}
                </Container>
                : <Box className={styles.container}><CircularProgress /></Box>}
        </Box>
    )
}
export default viewReport;