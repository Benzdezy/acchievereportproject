import { Container } from "@mui/system";
import Paper from '@mui/material/Paper';
import { IconButton } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import InputBase from '@mui/material/InputBase';
import { useEffect, useState } from "react";
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import { getCookie, setCookie } from "cookies-next";
import { useRouter } from 'next/router';
import CircularProgress from '@mui/material/CircularProgress';
import styles from '../../styles/Home.module.css'

const Setting = () => {

    const router = useRouter();
    const [alignment, setAlignment] = useState('ทั้งหมด');
    const [display, setDisplay] = useState(false);

    useEffect(() => {
        // const userId = getCookie('UserID');
        const accessToken = getCookie('AccessToken');
        const refreshToken = getCookie('RefreshToken');
        // const stringLIFF_STORE = localStorage.getItem('LIFF_STORE:1657831817-rzJgbMK6:context')
        // const jsonUID = JSON.parse(stringLIFF_STORE)
        // const userId = jsonUID.userId
        onVerifyProfile(accessToken, refreshToken)
    }, [])

    const onVerifyProfile = async (token, reToken) => {
        const options = {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`
            }
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/profile'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status onVerifyProfile => ', result.status)
        if (result.status == false) {
            refreshToken(reToken)
        } else if (result.status == true) {
            const dataUser = result.data.UserType
            console.log(dataUser)
            if (dataUser == "Customer") {
                console.log("Verify successfuly!! Customer")
                router.push('/usersetting') //อาจไม่จำเป็น
            } else if (dataUser == "Admin") {
                console.log("Verify successfuly!! Admin")
                router.push('/usersetting') //อาจไม่จำเป็น
            }
        }
    }

    const refreshToken = async (refreshToken) => {
        const data = {
            RefreshToken: `${refreshToken}`
        }
        const JSONdata = JSON.stringify(data)
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSONdata
        }
        const endpoint = 'https://api.lineoa.acchieve.com/acchiveReport/api/v1/authen/refreshToken'
        const response = await fetch(endpoint, options)
        const result = await response.json()
        console.log('status refreshToken => ', result.status)
        if (result.status == false) {
            console.log('refreshToken ==> status false')
        } else if (result.status == true) {
            await deleteCookie('AccessToken')
            await deleteCookie('RefreshToken')
            await setCookie('AccessToken', `${result.data.AccessToken}`)
            await setCookie('RefreshToken', `${result.data.RefreshToken}`)
            await onVerifyProfile(result.data.AccessToken, result.data.RefreshToken)
        }
    }

    const handleChange = (event, newAlignment) => {
        setAlignment(newAlignment);
    };

    return (
        <div>
            {display
                ? <Container maxWidth="lg" sx={{ justifyContent: 'center' }}>
                    <Paper
                        elevation={3}
                        component="form"
                        sx={{ justifyContent: 'center', mt: 2, p: 2, height: '95vh' }}
                    >
                        <Paper
                            sx={{ display: 'flex', borderRadius: 2 }}
                        >
                            <IconButton>
                                <SearchIcon />
                            </IconButton>
                            <InputBase
                                sx={{ flex: 1, borderRadius: 3, borderColor: 'gray', p: 0.5 }}
                                placeholder="ค้นหา..."
                                fullWidth

                            />
                        </Paper>
                        <ToggleButtonGroup
                            color="primary"
                            value={alignment}
                            exclusive
                            onChange={handleChange}
                            aria-label="Platform"
                            sx={{ mt: 2, borderRadius: 4 }}
                        >
                            <ToggleButton value="ลูกค้า">ลูกค้า</ToggleButton>
                            <ToggleButton value="แอดมิน">แอดมิน</ToggleButton>
                            <ToggleButton value="ผู้จัดการ">ผู้จัดการ</ToggleButton>
                            <ToggleButton value="ทั้งหมด">ทั้งหมด</ToggleButton>
                        </ToggleButtonGroup>
                        <h1>การตั้งค่า</h1>
                        {/* <TextField
                    label="With normal TextField"
                    InputProps={{
                        endAdornment: (
                            <InputAdornment>
                                <IconButton>
                                    <SearchIcon />
                                </IconButton>
                            </InputAdornment>
                        )
                    }}
                /> */}
                    </Paper>

                </Container>
                : <div className={styles.container}><CircularProgress /></div>}
        </div>
    )
}
export default Setting;