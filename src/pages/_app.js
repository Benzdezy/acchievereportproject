import '../styles/globals.css'
// import { ThemeProvider, createTheme } from '@mui/material'
// import { Sriracha } from '@next/font/google'

// const sriracha = Sriracha({
//     subsets: ['thai'],
//     weight: ['400']
// })

function MyApp({ Component, pageProps }) {
    // const theme = createTheme({
    //     typography: {
    //         fontFamily: [
    //             'Kanit', 'sans-serif',
    //         ].join(','),
    //     },
    // });
    return (
        // <main className={sriracha.className}>
        //     <ThemeProvider theme={theme}>
        //         < Component {...pageProps} />
        //     </ThemeProvider>
        // </main>
        < Component {...pageProps} />
    )
}

export default MyApp